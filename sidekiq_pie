#!/usr/bin/env ruby
# frozen_string_literal: true

require 'pty'
require 'tty-pie'
require 'json'
require 'pastel'

class PieChart
  SYMBOL = '•'

  def initialize(name:, x_coord:)
    @name = name
    @success_val = 0
    @error_val = 0

    @pie_chart = TTY::Pie.new(data: [], radius: 5, left: x_coord, top: 3, legend: { format: "%<label>s %<name>s %<value>d (%<percent>.2f%%)" })
  end

  def update!(success)
    success ? @success_val += 1 : @error_val += 1

    update
  end

  private

  def success
    { name: "#{@name} Success", value: @success_val, color: :bright_green, fill: SYMBOL }
  end

  def error
    { name: "#{@name} Error", value: @error_val, color: :bright_red, fill: SYMBOL }
  end

  def update
    @pie_chart.update([success, error])

    print @pie_chart
  end
end

class PieCmd
  CMD = "ssh -t #{ARGV[0]} tail -n 10000 -f /home/gitlab-customers/customers-gitlab-com/log/sidekiq.log | grep UpdateGitlabPlanInfoWorker".freeze

  def initialize(env)
    @env = env
    @pie = PieChart.new(name: 'UpdateGitlabPlanInfoWorker', x_coord: 1)
  end

  def run
    system("clear")

    puts Pastel.new.bright_white.on_blue("🚨 UpdateGitlabPlanInfoWorker status on #{@env} ")

    process_stream
  end

  private

  def process_stream
    begin
      PTY.spawn(CMD) do |stdout, _stdin, _pid|
        begin
          stdout.each do |line|
            next unless line&.include?('UpdateGitlabPlanInfoWorker')

            if line.include?('Job raised exception')
              @pie.update!(false)
              print_error(line)
            elsif line.include?('INFO: done')
              @pie.update!(true)
            else
              next
            end
          end
        rescue Errno::EIO
          puts "Error reading stream!"
        end
      end
    rescue PTY::ChildExited
      puts "Child process exited!"
    end
  end

  def print_error(line)
    print "\033[K "
    print "Latest error: #{line}" + ' ' * 100
  end
end

PieCmd.new(ARGV[0]).run
